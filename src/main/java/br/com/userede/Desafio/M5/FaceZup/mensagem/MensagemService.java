package br.com.userede.Desafio.M5.FaceZup.mensagem;

import br.com.userede.Desafio.M5.FaceZup.mensagem.dto.CadastroMensagemDTO;
import br.com.userede.Desafio.M5.FaceZup.usuario.Usuario;
import br.com.userede.Desafio.M5.FaceZup.usuario.UsuarioService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;


@Service
public class MensagemService {
    @Autowired
    private MensagemRepository mensagemRepository;
    @Autowired
    private UsuarioService usuarioService;

    public Mensagem cadastrarMensagem(String mensagem, String origem, String destino){

      Usuario emailOrigem = usuarioService.buscarUsuarioEmailId(origem);
       Usuario emailDestino = usuarioService.buscarUsuarioEmailId(destino);

       Mensagem objMensagem = new Mensagem();
       objMensagem.setMensagem(mensagem);
       objMensagem.setUsuarioOrigem(emailOrigem);
       objMensagem.setUsuarioDestino(emailDestino);
       objMensagem.setHora(LocalTime.now());
       objMensagem.setDataEnvio(LocalDate.now());
       objMensagem.setVisualizado(false);

    return mensagemRepository.save(objMensagem);
    }

    public Mensagem pesquisarMensagemId(Integer id) {

        Optional<Mensagem> mensagemOptional = mensagemRepository.findById(id);
        if(mensagemOptional.isPresent()){
            return mensagemOptional.get();
        }
        throw new RuntimeException("Mensagem não encontrada");
    }

    public Mensagem visualizarMensagemId(Integer id){

        Mensagem mensagem =  pesquisarMensagemId(id);
        if(mensagem == pesquisarMensagemId(id)){
            mensagem.setVisualizado(true);
            mensagemRepository.save(mensagem);

        }
        return mensagem;
    }

    public List<Mensagem> filtrarMensagemPor(String email, Boolean visualizado ){
        if(email != null){
            return mensagemRepository.findAllByVisualizado(visualizado);
        }if (visualizado != null){
            return mensagemRepository.findAllByUsuarioDestinoContains(email);
        }
        return (List<Mensagem>) mensagemRepository.findAll();
    }


     public  List<Mensagem> mensagensNãoLida(String email,Boolean visualizado ) {
       if (email != null){
           mensagemRepository.findAllByUsuarioDestinoContains(email);
       }
         if (visualizado != null) {
             mensagemRepository.findAllByVisualizado(visualizado);
         }
            return  (List<Mensagem>) mensagemRepository.findAll();
     }

}
