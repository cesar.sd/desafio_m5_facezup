package br.com.userede.Desafio.M5.FaceZup.usuario;

import br.com.userede.Desafio.M5.FaceZup.usuario.dto.CadastrarUsuarioDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;




@RestController
@RequestMapping("/usuarios")
public class UsuarioController {


    @Autowired
    private  UsuarioService usuarioService;
    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario CadastroUsuario(@RequestBody @Valid CadastrarUsuarioDTO usuarioDTO){
        usuarioService.usuarioExistente(usuarioDTO.getEmail());
        return usuarioService.salvarUsuario(modelMapper.map(usuarioDTO, Usuario.class));
    }


}
