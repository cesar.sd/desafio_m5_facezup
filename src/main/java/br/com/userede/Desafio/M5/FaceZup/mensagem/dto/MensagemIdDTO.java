package br.com.userede.Desafio.M5.FaceZup.mensagem.dto;

public class MensagemIdDTO {

    private int id;

    public MensagemIdDTO() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
