package br.com.userede.Desafio.M5.FaceZup.mensagem;

import br.com.userede.Desafio.M5.FaceZup.componete.Conversor;
import br.com.userede.Desafio.M5.FaceZup.mensagem.dto.*;
import br.com.userede.Desafio.M5.FaceZup.usuario.UsuarioService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/chat")
public class MensagemController {

    @Autowired
    private MensagemService mensagemService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private Conversor conversor;

  @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Mensagem cadastrarMensagem (@RequestBody CadastroMensagemDTO mensagemDTO ){
      return mensagemService.cadastrarMensagem(mensagemDTO.getMensagem(),mensagemDTO.getOrigem(),
              mensagemDTO.getDestino());
    }

    @GetMapping("/{mensagemId}")
    public VisualizarMensagemDTO visualizarMensagem(@PathVariable int mensagemId){
        Mensagem mensagem = mensagemService.visualizarMensagemId(mensagemId);
        return modelMapper.map(mensagem, VisualizarMensagemDTO.class);
    }

    @GetMapping("/usuario/perfil/{emailUsuario}")
    public List<MensagemIdDTO> pesquisarMensagem(@RequestParam(required = false)String emailUsuario,
                                                 @RequestParam(required = false)Boolean visualizado){
        List<Mensagem> mensagens = mensagemService.filtrarMensagemPor(emailUsuario, visualizado);
        List<MensagemIdDTO> mensagemIdDTOS = mensagens.stream().map(mensagem -> modelMapper.map(mensagem,MensagemIdDTO.class)).collect(Collectors.toList());
        return mensagemIdDTOS;
    }

     @GetMapping("/perfil/{emailUsuario}")
    public QuantidadeMensagemNãoLidoDTO quantidadeMensagemNãoLidoDTO (@PathVariable String email, @RequestParam Boolean visualizado ){
    QuantidadeMensagemNãoLidoDTO mensagem = new QuantidadeMensagemNãoLidoDTO();
    mensagem.setQuantidadeMensagemNãoLido(mensagemService.mensagensNãoLida(email, visualizado).size());
    return mensagem;
    }

}
