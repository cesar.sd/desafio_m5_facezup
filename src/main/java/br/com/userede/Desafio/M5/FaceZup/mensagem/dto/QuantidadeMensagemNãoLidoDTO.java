package br.com.userede.Desafio.M5.FaceZup.mensagem.dto;

public class QuantidadeMensagemNãoLidoDTO {

    private Integer QuantidadeMensagemNãoLido;

    public QuantidadeMensagemNãoLidoDTO() {
    }

    public Integer getQuantidadeMensagemNãoLido() {
        return QuantidadeMensagemNãoLido;
    }

    public void setQuantidadeMensagemNãoLido(Integer quantidadeMensagemNãoLido) {
        QuantidadeMensagemNãoLido = quantidadeMensagemNãoLido;
    }
}
