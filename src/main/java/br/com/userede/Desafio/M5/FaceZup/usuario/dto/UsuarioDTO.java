package br.com.userede.Desafio.M5.FaceZup.usuario.dto;

import br.com.userede.Desafio.M5.FaceZup.usuario.Usuario;

import javax.persistence.Id;

public class UsuarioDTO {
    private String nome;
    private String sobrenome;
    private String email;
    private String cargo;

    public UsuarioDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public static UsuarioDTO converterModelDTO (Usuario usuario){
        UsuarioDTO usuarioDTO = new UsuarioDTO();

        usuarioDTO.setNome(usuario.getNome());
        usuarioDTO.setSobrenome(usuario.getSobrenome());
        usuarioDTO.setEmail(usuario.getCargo());
        usuarioDTO.setCargo(usuarioDTO.getCargo());
        return usuarioDTO;

    }

}
