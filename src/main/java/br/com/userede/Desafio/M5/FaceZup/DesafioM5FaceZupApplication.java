package br.com.userede.Desafio.M5.FaceZup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioM5FaceZupApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioM5FaceZupApplication.class, args);
	}

}
