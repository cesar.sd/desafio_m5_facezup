package br.com.userede.Desafio.M5.FaceZup.usuario;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        return usuarioRepository.save(usuario);
    }

    public Usuario buscarUsuarioEmailId(String email){
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(email);

        if(usuarioOptional.isPresent()){
            return usuarioOptional.get();
        }

        throw new RuntimeException("Usuario não encontrado");

    }

    public boolean usuarioExistente(String email){
        //Optional<Usuario> usuario



        return usuarioRepository.existsById(email);
    }

    public void relatorio (String email ){


    }


}
