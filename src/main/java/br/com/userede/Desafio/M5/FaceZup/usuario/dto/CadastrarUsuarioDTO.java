package br.com.userede.Desafio.M5.FaceZup.usuario.dto;

import br.com.userede.Desafio.M5.FaceZup.usuario.Usuario;

import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class CadastrarUsuarioDTO {
    @Size(message = "Minimo com 2 letras", min = 2)
    private String nome;
    private String sobrenome;
    @Id
    @Email(message = "Não esta no padrão do Email")
    private String email;
    private String cargo;

    public CadastrarUsuarioDTO() {
    }

    public CadastrarUsuarioDTO(String nome, String sobrenome, String email, String cargo) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.cargo = cargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Usuario converterDTOemContato() {
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setSobrenome(sobrenome);
        usuario.setEmail(email);
        usuario.setCargo(cargo);

        return usuario;
    }

}
