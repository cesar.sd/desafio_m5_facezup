package br.com.userede.Desafio.M5.FaceZup.mensagem;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

@Repository
public interface  MensagemRepository extends CrudRepository <Mensagem, Integer> {
List<Mensagem> findAllByVisualizado(Boolean Visualizado);
List<Mensagem> findAllByUsuarioDestinoContains(String email);

  }