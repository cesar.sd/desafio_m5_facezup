package br.com.userede.Desafio.M5.FaceZup.mensagem.dto;

import br.com.userede.Desafio.M5.FaceZup.mensagem.Mensagem;

import java.time.LocalDate;

public class CadastroMensagemDTO {


    private String mensagem;
    private String origem;
    private String destino;


    public CadastroMensagemDTO() {
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

}

