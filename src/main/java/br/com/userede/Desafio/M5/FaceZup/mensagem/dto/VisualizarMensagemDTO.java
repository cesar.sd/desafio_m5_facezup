package br.com.userede.Desafio.M5.FaceZup.mensagem.dto;

import br.com.userede.Desafio.M5.FaceZup.usuario.Usuario;
import jdk.jfr.DataAmount;

import java.time.LocalDate;
import java.time.LocalTime;





public class VisualizarMensagemDTO {

    private String mensagem;
    private Usuario usuarioOrigem;
    private Usuario usuarioDestino;
    private LocalTime hora;
    private LocalDate dataEnvio;
    private boolean visualizado;

    public VisualizarMensagemDTO() {
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Usuario getUsuarioOrigem() {
        return usuarioOrigem;
    }

    public void setUsuarioOrigem(Usuario usuarioOrigem) {
        this.usuarioOrigem = usuarioOrigem;
    }

    public Usuario getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(Usuario usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public LocalTime getHora() {
        return hora;
    }

    public void setHora(LocalTime hora) {
        this.hora = hora;
    }

    public LocalDate getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(LocalDate dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }


}
